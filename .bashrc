export GPG_TTY="$(tty)"

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# PS1 (user input)
export PS1="\[$(tput bold)\]\w\[$(tput sgr0)\]\[\033[38;5;11m\]($(git branch 2>/dev/null | grep '^*' | colrm 1 2))\[$(tput sgr0)\]\[\033[38;5;3m\]\\$\[$(tput sgr0)\] \[$(tput sgr0)\]"

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# alias & scripts
source "$HOME"/.config/common-shell
[ -f "$HOME"/.config/common-shell-specific ] && source "$HOME"/.config/common-shell-specific

# fzf plugin
[ -f "$HOME"/.fzf.bash ] && source "$HOME"/.fzf.bash

# z plugin
[ -f /usr/share/z/z.sh ] && source /usr/share/z/z.sh

# pc specific
[ -f "$HOME"/.config/bashrc-specific ] && source "$HOME"/.config/bashrc-specific
[ -f "$HOME"/.config/common-shell-specific ] && source "$HOME"/.config/common-shell-specific

shopt -s checkwinsize # check window size after command
shopt -s histappend
shopt -s autocd # just type the directory
set -o vi # sets vim-like input

eval "$(zoxide init bash --cmd j)"
eval "$(fzf --bash)" # only 0.48.0 or later
