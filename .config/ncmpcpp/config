# vim: filetype=conf

# basic
ncmpcpp_directory = "~/.config/ncmpcpp"
lyrics_directory = "~/.local/share/lyrics"
mpd_host = "localhost"
mpd_port = "6600"
mpd_music_dir = "~/music"
mpd_connection_timeout = "5"
mpd_crossfade_time = "5"
message_delay_time = "1"

# colors
colors_enabled = "yes"
main_window_color = "white"
header_window_color = "cyan"
volume_color = "green"
statusbar_color = "white"
progressbar_color = "cyan"
progressbar_elapsed_color = "white"

# display modes
playlist_editor_display_mode = "columns"
search_engine_display_mode = "columns"
browser_display_mode = "columns"
playlist_display_mode = "columns"

# sing list
song_columns_list_format = "(30)[green]{t} (30)[magenta]{a} (30)[yellow]{p} (10)[blue]{l}"
song_list_format = "{$7%a - $9}{$5%t$9}|{$5%f$9}$R{$6%b $9}{$3%l$9}"
song_library_format = "{%n - }{%t}|{%f}"

# current item
current_item_prefix = "$(blue)$r"
current_item_suffix = "$/r$(end)"
current_item_inactive_column_prefix = "$(cyan)$r"

# alternative interface
user_interface = "alternative"
alternative_header_first_line_format = "$0$aqqu$/a {$6%a$9 - }{$3%t$9}|{$3%f$9} $0$atqq$/a$9"
alternative_header_second_line_format = "{{$4%b$9}{ [$8%y$9]}}|{$4%D$9}"

# classic interface
song_status_format = " $6%a $7⟫⟫ $3%t $7⟫⟫ $4%b "

# visualizer
visualizer_data_source = "/tmp/mpd.fifo"
visualizer_output_name = "Visualizer"
visualizer_in_stereo = "yes"
visualizer_type = "spectrum"
visualizer_look = "+|"

# navigation
cyclic_scrolling = "yes"
header_text_scrolling = "yes"
jump_to_now_playing_song_at_start = "yes"
lines_scrolled = "2"

# selected tracks
selected_item_prefix = "* "
discard_colors_if_item_is_selected = "yes"

# seeking
incremental_seeking = "yes"
seek_time = "1"

# visibility
header_visibility = "yes"
statusbar_visibility = "yes"
titles_visibility = "yes"

# now playing
now_playing_prefix = "> "
centered_cursor = "yes"

# browsing
current_item_inactive_column_suffix = "$/r$(end)"
media_library_primary_tag = "album_artist"
media_library_albums_split_by_date = no
startup_screen = "browser"
display_volume_level = no
ignore_leading_the = yes
external_editor = nvim
use_console_editor = yes
empty_tag_color = magenta
statusbar_time_color = cyan:b

# others
progressbar_look = "->"
system_encoding = "utf-8"
regular_expressions = "extended"
execute_on_song_change = notify-send "$(mpc --format '%title%' current)" "$(mpc --format '%artist%\n%performer%' current)"
autocenter_mode = "yes"
follow_now_playing_lyrics = "yes"
ignore_diacritics = "yes"
default_place_to_search_in = "database"
display_bitrate = "yes"
enable_window_title = "yes"
empty_tag_marker = ""

