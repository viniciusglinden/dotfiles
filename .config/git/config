[user]
    # not adding email to not push with wrong email
    # git config --local user.email "linden@viniciuslinden.xyz"
    name = Vinícius Gabriel Linden
    email = linden@viniciuslinden.xyz
[pull]
    rebase = true
[rebase]
    autoStash = true
[merge]
    conflictstyle = diff3
    tool = kdiff3
[core]
    excludesfile = ~/.config/git/ignore
    autocrlf = input
    safecrlf = false
[push]
    default = current
    autoSetupRemote = true
[alias]
    # note: it is not possible to override a command
    dot = "!git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"
    dota = "!f() { git dot ls-files --modified -z | xargs -0 git dot add; }; f"
    a = "add"
    s = "status"
    sw = "switch"
    c = "commit"
    can = "commit --amend --no-edit"
    ca = "commit --amend"
    co = "checkout"
    ours = "checkout --ours"
    theirs = "checkout --theirs"
    cr = "!f() { git clone --recursive $@ && echo Reminder to set the email; }; f"
    barework = "!bash ~/.config/git/bin/barework"
    b = "branch"
    d = "diff"
    rm = "rm -r"
    r = "restore"
    rs = "restore --staged"
    re = "rebase"
    w = "worktree"
    email = "config --local user.email"
    uncommit = "reset --sort HEAD~1"
    unstage = "reset HEAD --"
    undo = "reset --hard HEAD --"
    subinit = "submodule update --init --recursive"
    pf = "push --force-with-lease"
    # simple log
    lo = "log --oneline --decorate --graph --all"
    # affected files
    dn = "diff --name-only"
    # save to stash
    backup = "stash push -m"
    # get tag's commit
    tagcommit = "rev-list -n 1"
    # get current commit
    current = "rev-parse --verify HEAD"
    # get root folder canonical path
    root = "rev-parse --show-toplevel"
    last = "log -1 HEAD"
    alias = "config --get-regexp alias"
    # get the upstream name
    upstream = "rev-parse --abbrev-ref --symbolic-full-name @{upstream}"
    # clean repository
    klean = "clean -df"
    # cleanup merged branches on local
    bclean = "!f() { git fetch --all --prune && git branch --merged | grep -v '^*' | xargs git branch -d; }; f"
    # show file history
    filelog = "log -u"
    # inspect commit changes
    diffr  = "!f() { git diff "$1"^.."$1"; }; f"
    ours = "!f() { git checkout --ours $@ && git add $@; }; f"
    theirs = "!f() { git checkout --theirs $@ && git add $@; }; f"
    # set current upstream branch
    set = "!f() { branch=$(git rev-parse --abbrev-ref HEAD) && git branch -u origin/${branch} ${branch}; }; f"
    # add tracked
    at = "!f() { git ls-files --modified -z | xargs -0 git add; }; f"
    # beatify blame
    culpa = "blame --color-lines --color-by-age -w"
[filter "lfs"]
    required = true
    clean = git-lfs clean -- %f
    smudge = git-lfs smudge -- %f
    process = git-lfs filter-process
[blame]
    markIgnoredLines = true
    markUnblamableLines = true
[color]
    ui = auto
[init]
    defaultBranch = master
