function src_file() {
    [[ -f "${1}" ]] && source "${1}"
}

# plugin manager

[[ ! -d "${ZINIT_HOME:=${XDG_DATA_HOME:-${HOME}/.local/share}/zinit/zinit.git}" ]] &&
   mkdir -p "$(dirname -- ${ZINIT_HOME})" &&
   git clone https://github.com/zdharma-continuum/zinit.git "${ZINIT_HOME}"

source "${ZINIT_HOME}/zinit.zsh"

# completion

autoload -U compinit && compinit # load completions
zstyle ':completion:*' menu select
zmodload zsh/complist
_comp_options+=(globdots) # include hidden files

# modes

bindkey -v
bindkey '^k' vi-up-line-or-history
bindkey '^j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char
bindkey "^[[P" delete-char

function zle-keymap-select () {
    case "${KEYMAP}" in
        vicmd) echo -ne '\e[1 q';; # block
        viins|main) echo -ne '\e[5 q';; # beam
    esac
}
zle -N zle-keymap-select
zle-line-init() {
    # initiate `vi insert` as keymap
    # can be removed if `bindkey -V` has been set elsewhere
    zle -K viins
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # use beam shape cursor on startup.
preexec() {
    # beam shape cursor for each new prompt
    echo -ne '\e[5 q'
}
{
    # open current command line in vim (normal mode + space)
    autoload edit-command-line
    zle -N edit-command-line
    bindkey -M vicmd ' ' edit-command-line
}

# fixes

zle_highlight=('paste:none')
stty stop undef # disable CTRL-S terminal freeze
bindkey "^?" backward-delete-char # https://git.io/J48z9

# keybinding

bindkey '^R' history-incremental-search-backward # enable reverse-i-search
bindkey "^H" run-help
bindkey '^F' forward-word
bindkey '^B' backward-word
{
    # disabling keys
    bindkey -s "^[[4~" "" # end
    bindkey -s "^[[H" "" # home
    bindkey -s "^[[6~" "" # page down
    bindkey -s "^[[5~" "" # page up
    bindkey -M vicmd -s ":" ""
}

# alias & scripts

src_file "${HOME}/.config/common-shell"
src_file "${HOME}/.config/common-shell-specific"
src_file "${HOME}/.config/zsh-specific"

# settings

export GPG_TTY="$(tty)"
export KEYTIMEOUT=1
export HISTFILE=${ZDOTDIR:-$HOME}/.zsh_history
export HISTSIZE=10000
export SAVEHIST=${HISTSIZE}
export HISTDUP=erase
setopt auto_cd # just type the directory's name
setopt no_case_glob # case insensitive globbing
setopt no_extended_history # dont add timestamp and more to shell's history
setopt share_history # share history across multiple sessions
setopt append_history # append to history
setopt hist_ignore_space # remove leading space
setopt inc_append_history # dont wait until zsh exists to write commands to hist
setopt hist_expire_dups_first # expire duplicates first
setopt hist_ignore_all_dups # do not store duplications
setopt hist_ignore_dups # do not store duplications
setopt hist_find_no_dups # ignore duplicates when searching
setopt hist_reduce_blanks # removes blank lines from history
setopt hist_verify # expand with !! cmd
setopt hist_no_store # dont store `history`
setopt correct # typing corrections
setopt correct_all # typing corrections
setopt interactive_comments # allow comments in interactive shell
setopt prompt_subst # allow substitutions in PS1 (used for git substitutions)
setopt rm_star_silent # dont ask if I want to remove everything

# prompt

autoload -Uz vcs_info
autoload -U colors && colors
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:git*+set-message:*' hooks git-untracked
+vi-git-untracked() {
    [[ "$(git rev-parse --is-inside-work-tree 2>/dev/null)" == 'true' ]] &&
        git status --porcelain | grep '??' &> /dev/null &&
        hook_com[staged]+='!' # new files = !
}
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:git:*' formats "%{$fg[magenta]%}%b %{$fg[red]%}%m%u%c%{$fg[yellow]%}%{$reset_color%} "
last_return_code() {
    local ret="${?}"
    last_return_code_msg=""
    [[ "${ret}" -ne 0 ]] &&
        last_return_code_msg="%B%{$fg[red]%}${ret}%b "
}
precmd_functions+=(vcs_info last_return_code)
PROMPT="\$last_return_code_msg%B%~%F{3}%#%b%{${reset_color}%} \$vcs_info_msg_0_"
[[ "${SSH_CLIENT}" ]] && PROMPT="%B%m ${PROMPT}"

# plugins

command_not_found_handler() {
    command -v pacman &>/dev/null || return 0

    echo "Command not found, searching for package..."

    package_name=$(pacman -Fq "/usr/bin/${1}" | head)

    [[ -z "${package_name}" ]] &&
        echo "zsh: command not found: ${1}" &&
        exit 1

    echo -e "Command '\e[1m$1\e[0m' not found, but was found in the '\e[1m${package_name}\e[0m' package."
    echo -n "Would you like to install it? [y/N] "
    read -k confirm
    echo "\n"

    [[ "${confirm}" =~ [yY] ]] &&
        sudo pacman -S "${package_name##*/}" ||
        return 0

    echo "Running: ${@}"
    eval "${@}"
}

# eval "$(pip3 completion --zsh)"
eval "$(zoxide init zsh --cmd j)"
source <(fzf --zsh) # only 0.48.0 or later

# zstyle ':fzf-tab:complete:*' fzf-bindings \
#        'tab:select'
# zstyle ':fzf-tab:complete:(\\|*/|)man:*' fzf-preview 'man $word'
# zstyle ':fzf-tab:complete:cd:*' fzf-preview 'eza -1 --color=always $realpath'
#
# # disable sort when completing `git checkout`
# zstyle ':completion:*:git-checkout:*' sort false
# # set descriptions format to enable group support
# # NOTE: don't use escape sequences here, fzf-tab will ignore them
# zstyle ':completion:*:descriptions' format '[%d]'
# # set list-colors to enable filename colorizing
# zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
# # force zsh not to show completion menu, which allows fzf-tab to capture the unambiguous prefix
# zstyle ':completion:*' menu no
# # preview directory's content with eza when completing cd
# zstyle ':fzf-tab:complete:cd:*' fzf-preview 'eza -1 --color=always $realpath'
# # switch group using `<` and `>`
# zstyle ':fzf-tab:*' switch-group '<' '>'

src_file /usr/share/git/completion/git-prompt.sh

zinit light zdharma-continuum/fast-syntax-highlighting
zinit light zsh-users/zsh-autosuggestions
bindkey '^O' autosuggest-accept
zinit light Aloxaf/fzf-tab
zinit light Freed-Wu/fzf-tab-source # FIX

# TODO make it download grc if not available
zinit snippet OMZP::grc
