--[[
-- references:
-- https://github.com/sxyazi/yazi/tree/main/yazi-plugin/preset/components
-- https://yazi-rs.github.io/docs/plugins/overview
--
-- A tab is a vertical section
--
-- Root: container for Header, Tab and Status
--  Header: top bar
--  Status: lower status bar
--  Tab: contains all tabs and the rails
--   Parent: left tab
--   Current: middle tab
--   Preview: right tab, rendered via builtin utils (?)
--   Rail: sections separator
--
-- Entity: lines, contains Linemode, contained by Parent, Current and Preview
--  Linemode: right side on the lines in the main section
--]]
--

require("full-border"):setup {
    type = ui.Border.PLAIN
}
require("git"):setup()
require("relative-motions"):setup {
    show_numbers = "relative_absolute",
    show_motion = true
}

-- file status of the right side of the file
function Linemode:size_and_mtime()
    local time = math.floor(self._file.cha.mtime or 0)
    local size = self._file:size()

    if time > 0 and (os.date("%Y", time) == os.date("%Y")) then
        return ui.Line(string.format(" %s %s ", size and ya.readable_size(size) or "", os.date("%d %b", time)))
    end

    return ui.Line(string.format(" %s %s ", size and ya.readable_size(size) or "",
        time and os.date("%Y-%m-%d", time) or ""))
end

-- user/group of files in status bar
Status:children_add(function()
    local h = cx.active.current.hovered
    if h == nil or ya.target_family() ~= "unix" then
        return ui.Line {}
    end

    return ui.Line {
        ui.Span(ya.user_name(h.cha.uid) or tostring(h.cha.uid)):fg("magenta"),
        ui.Span(":"),
        ui.Span(ya.group_name(h.cha.gid) or tostring(h.cha.gid)):fg("magenta"),
        ui.Span(" "),
    }
end, 500, Status.RIGHT)

-- username and hostname in header
Header:children_add(function()
    if ya.target_family() ~= "unix" then
        return ui.Line {}
    end
    return ui.Span(ya.user_name() .. "@" .. ya.host_name() .. ":"):fg("blue")
end, 500, Header.LEFT)
