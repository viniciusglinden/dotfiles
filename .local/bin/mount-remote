#!/bin/bash -e
# Author: Vinícius Gabriel Linden
# Date: 2021-07-26
# Description: General purpose script to mount.
#  Apple Filing Protocol
#  SAMBA
#  The script looks into the $XDG_CACHE_HOME/mount-remote for a file syntax like the
#  following:
#  ```
#  type (= SAMBA, AFP)
#  location
#  user
#  password
#  mount place (relative to current directory)
#  ```
#  Password line can also be a command to decrypt a file.
#  Always use full paths

possible_mounts="SAMBA AFP"

# reading file through state machine
i="type"
while read line
do
	case $i in
		type)
			remote_type=$line
			i="location"
			;;
		location)
			location=$line
			i="user"
			;;
        user)
            user="${line}"
			i="password"
            ;;
		password)
			password=$line
			i="mount_place"
			;;
		mount_place)
            mount_place="$(echo "${line}" | sed 's/\~/$HOME/' | envsubst)"
			i="TODO"
			;;
		TODO)
			echo "TODO: implement multiple mount places"
			;;
		*)
			echo "Line processing went wrong!"
			exit 127
			;;
	esac
done < $XDG_CACHE_HOME/mount-remote

# testing password for encryption or plain text
test_password=($(echo $password | tr " " "\n"))
which ${test_password[0]} >/dev/null 2>&1
if [ $? -eq 0 ]; then
	echo "Password is encrypted"
	echo "Decrypting password..."
	password=$(sh -c "$password" 2>/dev/null)
	[ $? -ne 0 ] && echo "Decryption failed! Aborting everything!" && exit 127
fi

# test mount place directory
[ ! -d "${mount_place}" ] && mkdir -p "${mount_place}"
if [ "$(ls "$mount_place" | wc -l)" -ne 0 ]; then
    echo "$mount_place exists and it is not empty - aborting now!"
    exit
fi

echo "$remote_type"

case $remote_type in
	SAMBA)
		sudo mount -t cifs \
            -o uid="${USER}",gid=users,username="${user}",password=$password $location $mount_place
		;;
	AFP)
		daemon_running=$(ps aux | grep afpfsd | wc -l)
		[ ! "$daemon_running" -eq 2 ] && \
			(afpfsd || exit) || echo "Daemon is already running"
		sudo mount_afp afp://"${user}":$password@$location $mount_place
		;;
	*)
		echo "$remote_type is not supported"
		exit 127
		;;
esac

echo "Remote folders mounted at ${mount_place}"

echo "mounted" > /tmp/remote-mount

