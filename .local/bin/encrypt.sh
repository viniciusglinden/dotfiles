#!/bin/bash
# author Vinícius Gabriel Linden
# date 2022-04-30
# brief Encrypt and delete original file.

gpg -c ${1} && shred -u ${1}
